import datetime

# from croppie.fields import CroppieField
import re

from croppie.fields import CroppieField
from django import forms
from django.contrib.auth import get_user_model
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext as _
# from multiupload.fields import MultiImageField

# from general.models import GeneralPermission
# from panel.models import *
from image_cropping import ImageCropWidget

from content.models import Section, Education, WorkExperience

GUser = get_user_model()


class PostShareForm(forms.Form):
    text = forms.CharField(max_length=5000,label=_('Mətn'), required=True, widget=forms.Textarea(attrs={ 'autocomplete': 'off','class': 'form-control','rows': '3', }))


class MainSearchForm(forms.Form):
    search_text = forms.CharField(max_length=255,label=_('Axtar'), required=False, widget=forms.TextInput(
        attrs={'placeholder': _('Axtar'), 'autocomplete': 'off',
               'class': 'form-control', }))


class SectionMainSearchForm(forms.Form):
    search_text = forms.CharField(max_length=255,label=_('Axtar'), required=False, widget=forms.TextInput(
        attrs={'placeholder': _('Axtar'), 'autocomplete': 'off',
               'class': 'form-control', }))



class ProfileSettingsForm(forms.Form):
    name = forms.CharField(required=True,max_length=254, label="Ad", error_messages={})
    surname = forms.CharField(required=True,max_length=254, label="Soyad", error_messages={})
    email = forms.EmailField(required=True,error_messages={},label=_('Email'))
    phone = forms.CharField(required=False,error_messages={},label=_('Nömrə'))
    facebook = forms.CharField(required=False,error_messages={},label=_('Facebook'))
    linkedin = forms.CharField(required=False,error_messages={},label=_('Linkedin'))
    instagram = forms.CharField(required=False,error_messages={},label=_('Instagram'))
    about = forms.CharField(required=False,error_messages={},label=_('Haqqında'))
    sections = forms.MultipleChoiceField(required=False,error_messages={},label=_('Bölmələr'))
    birthdate = forms.DateField(input_formats=('%d.%m.%Y',),label=_('Doğum tarixi'), required=False, widget=forms.DateInput(format='%d.%m.%Y',attrs={'autocomplete': 'off','class': 'form-control'}))
    password = forms.CharField(required=False,label=_("Şifrə"))
    retype_password = forms.CharField(required=False,label=_("Şifrə təkrarı"))

    user_id = 0

    def __init__(self,user_id, *args, **kwargs):
        super(ProfileSettingsForm, self).__init__(*args, **kwargs)
        self.user_id = user_id
        self.fields['name'].widget = forms.TextInput(attrs={
            'class':'form-control'})
        self.fields['surname'].widget = forms.TextInput(attrs={
            'class':'form-control'})
        self.fields['email'].widget = forms.EmailInput(attrs={
            'class':'form-control'})
        self.fields['phone'].widget = forms.TextInput(attrs={
            'class':'form-control'})
        self.fields['facebook'].widget = forms.TextInput(attrs={
            'class':'form-control'})
        self.fields['linkedin'].widget = forms.TextInput(attrs={
            'class':'form-control'})
        self.fields['instagram'].widget = forms.TextInput(attrs={
            'class':'form-control'})
        self.fields['about'].widget = forms.Textarea(attrs={
            'class':'form-control','rows':3})
        self.fields['password'].widget = forms.PasswordInput(attrs={
            'class':'form-control'})
        self.fields['retype_password'].widget = forms.PasswordInput(attrs={'class':'form-control'})
        self.fields['sections'].widget = forms.CheckboxSelectMultiple(attrs={'class':'form-control'})
        self.fields['sections'].choices = [['', 'Maraq dairələri']] + [[x.id, x.title] for x in Section.objects.filter()]


    def clean(self):
        cleaned_data = super(ProfileSettingsForm, self).clean()

        email = cleaned_data.get('email')
        password = cleaned_data.get('password')
        password_confirm = cleaned_data.get('retype_password')

        user_email_obj = GUser.objects.filter(email=email).exclude(id=self.user_id)
        if user_email_obj:
            self._errors['email'] = _('Email artıq istifadə olunub')
        if user_email_obj:
            self._errors['username'] = _('Email artıq istifadə olunub')
        if password:
            if password and password_confirm:
                if password != password_confirm:
                    raise forms.ValidationError(_("Şifrələr eyni deyil"))
        return cleaned_data


class EducationForm(forms.ModelForm):

    # birth_date = forms.DateField(widget=DateTimePicker(options={"format": "YYYY-MM-DD", "pickSeconds": False}))
    class Meta:
        model = Education
        fields = ('title', 'university', 'start_date', 'end_date',)
        # exclude = ('user',)
class WorkExperienceForm(forms.ModelForm):

    # birth_date = forms.DateField(widget=DateTimePicker(options={"format": "YYYY-MM-DD", "pickSeconds": False}))
    class Meta:
        model = WorkExperience
        fields = ('title', 'company', 'start_date', 'end_date',)
        # exclude = ('user',)


from PIL import Image
from django import forms
from django.core.files import File
from .models import Photo


class PhotoForm(forms.ModelForm):
    image = CroppieField(
        required=False,
        options={
            'viewport': {
                'width': 400,
                'height': 380,
            },
            'boundary': {
                'width': 480,
                'height': 420,
            },
            'showZoomer': True,
            'enableOrientatio': True
        },
    )
    class Meta:
        model = Photo
        fields = ('image',)
        widgets = {
            'image': ImageCropWidget,
        }


# class PhotoForm(forms.ModelForm):
#     x = forms.FloatField(widget=forms.HiddenInput())
#     y = forms.FloatField(widget=forms.HiddenInput())
#     width = forms.FloatField(widget=forms.HiddenInput())
#     height = forms.FloatField(widget=forms.HiddenInput())
#
#     class Meta:
#         model = Photo
#         fields = ('file', 'x', 'y', 'width', 'height', )
#     def datatet(self):
#         pass
#
#     def save(self):
#         photo = super(PhotoForm, self).save()
#
#         x = self.cleaned_data.get('x')
#         y = self.cleaned_data.get('y')
#         w = self.cleaned_data.get('width')
#         h = self.cleaned_data.get('height')
#
#         image = Image.open(photo.file)
#         cropped_image = image.crop((x, y, w+x, h+y))
#         resized_image = cropped_image.resize((200, 200), Image.ANTIALIAS)
#         resized_image.save(photo.file.path)
#
#         return photo