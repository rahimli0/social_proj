from django.urls import path

from userprofile.views import *

app_name = 'userprofile'

urlpatterns = [
	path('', feed, name='feed'),
	path('', feed, name='dashboard'),
	path('photo-upload/', photo_upload, name='photo-upload'),
	path('page/<slug>/', staticpage_detail, name='static-page-detail'),
	path('get-photo-profile/<type>/', get_photo_profile, name='get-photo-profile'),
	# path('profile/', profile, name='profile'),
	# path('profile/<slug>', profile_tab, name='profile-tab'),
	path('v/<id>/', profile_v, name='profile-v'),
	path('v/<id>/change/', profile_v_change, name='profile-v-change'),
	path('general-post-change/', general_post_change, name='general-post-change'),
	path('v/<id>/<slug>', profile_tab, name='profile-tab-v'),
	path('settings/<slug>', settings_page, name='settings'),
	path('experience-andeducation-create/<slug>', experience_andeducation_create, name='experience-andeducation-create'),
	path('experience-andeducation-list/<slug>', experience_andeducation_list, name='experience-andeducation-list'),
	path('post-share/', post_share, name='post-share'),
	path('post-remove/<id>/', post_remove, name='post-remove'),

	path('like-save/<int:id>/<slug>/', like_save, name='like-save-post'),



	path('friend-list/<slug>', friend_list, name='friend-list'),
	# path('projects-form/', projects_form, name='projects-form'),
	path('notifications-list/', notifications_list, name='notifications-list'),
	path('message-notification-show/<slug>/', message_notification_show, name='message-notification-show'),
	path('notification-remove/<id>/', notification_remove, name='notification-remove'),
	path('users-list/', users_list, name='users-list'),
	path('event-list/<slug>/', event_list, name='event-list'),
	path('news-list/<slug>/', blognews_list, name='blognews-list'),
	path('friend-list/<slug>/', friend_list, name='friend-list'),
	path('all-sections/', all_sections, name='all-sections'),
	path('all-sections/section-<int:id>/', section_item, name='section-item'),
	path('all-sections/section-<int:id>/parent-<int:p_id>/', tutorials, name='section-item-parent'),
	path('all-sections/tutorial-<int:id>/', article_item, name='tutorial-item'),

	path('projects-list/', projects_list, name='projects-list'),
	path('projects/create/', projects_create, name='projects-create'),
	path('projects/edit/<int:id>/', projects_edit, name='projects-edit'),
	path('projects/view/<int:id>/', projects_view, name='projects-view'),

	path('events-list/', events_list, name='events-list'),
	path('events/view/<int:id>/', events_view, name='events-view'),
	path('news-list/', news_list, name='news-list'),
	path('news/edit/<int:id>/', projects_edit, name='news-view'),


 ]