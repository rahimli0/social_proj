import hashlib
import random

from django.forms import inlineformset_factory
from django.views import View

from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import make_password
from django.db.models import Q
from django.http import JsonResponse, Http404
from django.shortcuts import render, get_object_or_404

from django.utils.translation import ugettext as _
# Create your views here.
from django.template.loader import render_to_string
from django.utils import timezone

from base_user.tools.common import USERTYPESSEARCH
from chat.models import Thread
from content.forms import ProjectModelForm
from content.models import *
from general.views import base_auth
from userprofile.forms import *

GUser = get_user_model()

@login_required(login_url='base-user:login')
def photo_upload(request):
    context = base_auth(req=request)
    photos = Photo.objects.all()
    if request.method == 'POST':
        form = PhotoForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            # return redirect('photo_list')
    else:
        form = PhotoForm()
    context['form'] = form
    return render(request, 'userprofile/photo/profile.html', context)

@login_required(login_url='base-user:login')
def feed(request):
    now = datetime.datetime.now()
    user = request.user
    context = base_auth(req=request)

    context['post_share_form'] = PostShareForm(request.POST or None)
    if request.method == 'POST' and request.is_ajax():
        try:
            page_num = int(request.POST.get('post-page-num', 1))
        except:
            page_num = 0
        try:
            loaded_post_count = int(request.POST.get('loaded-post-count', 1))
        except:
            loaded_post_count = 0
        posts = SharedPost.objects.filter().order_by('-id')

        user_liked = SharedPostLike.objects.filter(user=user).values_list('post_id',flat=True)
        posts = posts[50 * (page_num - 1) + loaded_post_count:50 * page_num + loaded_post_count]
        list_html = ''
        last_item  = None
        for_i = 0
        for item in posts:
            if for_i == 0 and page_num == 1:
                last_item = item
            for_i +=1
            list_html = "{}{}".format(list_html, render_to_string(
                "home/include/post/post-item.html",
                {
                    "post_liked": True if item.id in user_liked else False,
                    "item": item,
                    "me": True if request.user == item.user else False,
                }))
        data = {
            'main_result':list_html,
            'date_time':last_item.id if last_item else 0,
        }
        return JsonResponse(data=data)
    context['last_events'] =  Event.objects.filter()[:10]
    context['last_blogs'] =  BlogNews.objects.filter()[:10]
    return render(request, 'home/index.html', context=context)


@login_required(login_url='base-user:login')
def post_share(request):
    now = datetime.datetime.now()
    context = base_auth(req=request)
    user = request.user
    form = PostShareForm(request.POST or None,request.FILES or None)
    if request.method == 'POST' and  request.is_ajax():
        message_code = 0
        message = 0
        if form.is_valid():
            cleaned_data = form.cleaned_data
            text = cleaned_data.get('text',None)
            data_item = SharedPost(content=text,user=user)
            data_item.save()
            message_code = 1
            message = 'Uğurlu'
        else:
            message = " , ".join([v[0].__unicode__() for k, v in form.errors.items()])

        data = {
            'code':message_code,
            'result':message,
        }
        return JsonResponse(data=data)

    else:
        raise Http404


@login_required(login_url='base-user:login')
def like_save(request,id,slug):
    now = datetime.datetime.now()
    context = base_auth(req=request)
    user = request.user
    form = PostShareForm(request.POST or None,request.FILES or None)

    if request.method == 'POST' and  request.is_ajax():
        message_code = 0
        message = 0
        liked_count = 0
        saved_count = 0
        if slug == 'liked':
            try:
                data_item = SharedPostLike.objects.filter(user=user,post_id=id).get()
                data_item.delete()
                data_item.post.liked_count = data_item.post.liked_count - 1
                data_item.post.save()
                message_code = 2
            except:
                try:
                    data_item = SharedPostLike.objects.create(user=user,post_id=id)
                    data_item.post.liked_count = data_item.post.liked_count + 1
                    data_item.post.save()
                    message_code = 1
                    saved_count = data_item.post.saved_count
                    liked_count = data_item.post.liked_count
                except:
                    pass
            data = {
                'code':message_code,
                'saved_count':saved_count,
                'liked_count':liked_count,
            }
            return JsonResponse(data=data)
        else:
            raise Http404


    else:
        raise Http404


def staticpage_detail(request,slug):
    now = datetime.datetime.now()
    context = base_auth(req=request)

    # return '\n'.join(url.format(year) for year in range(2000, 2016))
    context['data_item'] =  get_object_or_404(StaticPage,key_page=slug)
    # context['mac_address'] =  Package.objects.filter(status__in=['active','waiting']).count()
    return render(request, 'home/page.html', context=context)


@login_required(login_url='base-user:login')
def profile(request):
    now = datetime.datetime.now()
    context = base_auth(req=request)
    user = request.user

    # return '\n'.join(url.format(year) for year in range(2000, 2016))
    # context['mac_address'] =  ':'.join(("%012X" % mac)[i:i+2] for i in range(0, 12, 2))
    # context['mac_address'] =  Package.objects.filter(status__in=['active','waiting']).count()
    context['uud_id'] = user.uuid_id
    return render(request, 'userprofile/dashboard.html', context=context)


@login_required(login_url='base-user:login')
def profile_v(request,id):
    now = datetime.datetime.now()
    context = base_auth(req=request)
    user = get_object_or_404(GUser,uuid_id=id)
    current_user_ch = False
    if str(request.user.uuid_id) == id:
        current_user_ch = True
    context['uud_id'] = id


    if request.method == 'POST' and request.is_ajax():
        try:
            page_num = int(request.POST.get('post-page-num', 1))
        except:
            page_num = 0
        try:
            loaded_post_count = int(request.POST.get('loaded-post-count', 1))
        except:
            loaded_post_count = 0
        posts = SharedPost.objects.filter(user=user).order_by('-id')

        user_liked = SharedPostLike.objects.filter(user=user).values_list('post_id',flat=True)
        posts = posts[50 * (page_num - 1) + loaded_post_count:50 * page_num + loaded_post_count]
        list_html = ''
        last_item  = None
        for_i = 0
        for item in posts:
            if for_i == 0 and page_num == 1:
                last_item = item
            for_i +=1
            list_html = "{}{}".format(list_html, render_to_string(
                "home/include/post/post-item.html",
                {
                    "post_liked": True if item.id in user_liked else False,
                    "item": item,
                    "me": True if request.user == item.user else False,
                }))
        data = {
            'main_result':list_html,
            'extra_count':posts.count(),
            'date_time':last_item.id if last_item else 0,
        }
        return JsonResponse(data=data)
    # return '\n'.join(url.format(year) for year in range(2000, 2016))
    # context['mac_address'] =  ':'.join(("%012X" % mac)[i:i+2] for i in range(0, 12, 2))
    # context['mac_address'] =  Package.objects.filter(status__in=['active','waiting']).count()
    context['user_profile'] = user
    context['current_user_ch'] = current_user_ch
    context['post_share_form'] = PostShareForm(request.POST or None)
    return render(request, 'userprofile/dashboard.html', context=context)


@login_required(login_url='base-user:login')
def post_remove(request,id):
    now = datetime.datetime.now()
    context = base_auth(req=request)
    user = request.user
    current_user_ch = False
    if request.method == 'POST' and request.is_ajax():
        try:
            post_item = SharedPost.objects.get(id=id,user=user)
            post_item.delete()
            code = 1
        except:
            code = 0
        data = {
            'code' : code,
        }
        return JsonResponse(data)
    else:
        raise Http404

@login_required(login_url='base-user:login')
def profile_v_change(request,id):
    now = datetime.datetime.now()
    context = base_auth(req=request)
    user = get_object_or_404(GUser,uuid_id=id)
    current_user_ch = False
    if str(request.user.uuid_id) == id:
        pass
    context['uud_id'] = id



    if request.method == 'POST' and request.is_ajax():
        list_html = ''
        date_time = -1
        extra_count = 0
        try:
            try:
                date_time = request.POST.get('date_time', 1)
            except:
                date_time = -1
            posts = SharedPost.objects.filter(id__gt=date_time).order_by('-id')

            if posts:
                code = 1
                extra_count = posts.count()
                user_liked = SharedPostLike.objects.filter(post_id__in=posts.values_list('id',flat=True)).filter(user=user).values_list('post_id',flat=True)
                last_item  = None
                for_i = 0
                if posts:
                    for item in posts:
                        if for_i == 0:
                            last_item = item
                        for_i += 1
                        list_html = "{}{}".format(list_html, render_to_string(
                            "home/include/post/post-item.html",
                            {
                                "post_liked": True if item.id in user_liked else False,
                                "item": item,
                                "me": True if request.user == item.user else False,
                            }))
                    date_time = last_item.id
            else:
                code = 2

        except:
            code = 0
        data = {
            'main_result':list_html,
            'date_time':date_time,
            'extra_count':extra_count,
            'code':code,
        }
        return JsonResponse(data=data)



@login_required(login_url='base-user:login')
def general_post_change(request):
    now = datetime.datetime.now()
    user = request.user


    if request.method == 'POST' and request.is_ajax():
        list_html = ''
        date_time = -1
        extra_count = 0
        try:
            try:
                date_time = request.POST.get('date_time', 1)
            except:
                date_time = -1
            print("--------------------------------- date_time = {} ".format(date_time))
            posts = SharedPost.objects.filter(id__gt=date_time).order_by('-id')

            if posts:
                code = 1
                extra_count = posts.count()
                user_liked = SharedPostLike.objects.filter(post_id__in=posts.values_list('id',flat=True)).filter(user=user).values_list('post_id',flat=True)
                last_item  = None
                for_i = 0
                if posts:
                    for item in posts:
                        if for_i == 0:
                            last_item = item
                        for_i += 1
                        list_html = "{}{}".format(list_html, render_to_string(
                            "home/include/post/post-item.html",
                            {
                                "post_liked": True if item.id in user_liked else False,
                                "item": item,
                                "me": True if request.user == item.user else False,
                            }))
                    date_time = posts.first().id
            else:
                code = 2

        except:
            code = 0
        data = {
            'main_result':list_html,
            'date_time':date_time,
            'extra_count':extra_count,
            'code':code,
        }
        return JsonResponse(data=data)


@login_required(login_url='base-user:login')
def profile_tab(request,id,slug):
    now = datetime.datetime.now()
    context = base_auth(req=request)

    context['slug'] = slug
    context['uud_id'] = id
    result_data = ''
    user = get_object_or_404(GUser,uuid_id=id)
    current_user_ch = False
    if str(request.user.uuid_id) == id:
        current_user_ch = True
    if slug == 'expreience':
        page_title = _('Expreience')
        experiences = WorkExperience.objects.filter(user=user)
        for item in experiences:
            result_data = "{}{}".format(result_data, render_to_string(
                "userprofile/include/experience-item.html",
                {
                    "data_item": item,
                }))
    elif slug == 'education':
        page_title = _('Education')
        experiences = Education.objects.filter(user=user)
        for item in experiences:
            result_data = "{}{}".format(result_data, render_to_string(
                "userprofile/include/education-item.html",
                {
                    "data_item": item,
                }))
    elif slug == 'about':
        page_title = _('About')
        result_data = "{}{}".format(result_data, render_to_string(
            "userprofile/include/about.html",
            {
                "data_item": user,
            }))
    else:
        raise Http404
    # return '\n'.join(url.format(year) for year in range(2000, 2016))
    # context['mac_address'] =  ':'.join(("%012X" % mac)[i:i+2] for i in range(0, 12, 2))
    context['result_data'] =  result_data
    context['page_title'] =  page_title
    context['user_profile'] = user
    context['current_user_ch'] = current_user_ch
    return render(request, 'userprofile/profile-tab.html', context=context)


from django.forms.models import modelformset_factory
@login_required(login_url='base-user:login')
def settings_page(request, slug):
    user_obj = request.user

    now = datetime.datetime.now()
    context = base_auth(req=request)
    if slug == 'profile':

        user_obj = request.user
        inital_data = {
            'name': user_obj.first_name,
            'surname': user_obj.last_name,
            'email': user_obj.email,
            'facebook': user_obj.facebook,
            'linkedin': user_obj.linkedin,
            'instagram': user_obj.instagram,
            'about': user_obj.about,
            # 'phone': user_obj.phone,
        }
        profileSettingsForm = ProfileSettingsForm(user_obj.id, request.POST or None, request.FILES or None, initial=inital_data)
        context['profileSettingsForm'] = profileSettingsForm

        context = base_auth(req=request)
        if request.method == 'POST':
            if profileSettingsForm.is_valid():
                clean_data = profileSettingsForm.cleaned_data
                name = clean_data.get('name', None)
                surname = clean_data.get('surname', None)
                email = clean_data.get('email', None)
                facebook = clean_data.get('facebook', None)
                linkedin = clean_data.get('linkedin', None)
                instagram = clean_data.get('instagram', None)
                about = clean_data.get('about', None)
                # phone = clean_data.get('phone', None)
                random_string = str(random.random()).encode('utf8')
                salt = hashlib.sha1(random_string).hexdigest()[:5]

                # activation_key = hashlib.sha1(salted).hexdigest()
                #
                # key_expires = datetime.datetime.today() + datetime.timedelta(1)

                user_obj.first_name = name
                if facebook:
                    user_obj.facebook = facebook
                if linkedin:
                    user_obj.linkedin = linkedin
                if instagram:
                    user_obj.instagram = instagram
                if about:
                    user_obj.about = about
                user_obj.last_name = surname
                user_obj.email = email
                user_obj.username = email
                # user_obj.phone = phone
                user_obj.save()
                messages.success(request, _('Succesfully changed'))
                # profileSettingsForm = EmployeeEditForm(user_id=user_obj.id)
            # else:
            #     return HttpResponse(profileSettingsForm.errors)

        context['profileSettingsForm'] = profileSettingsForm
        context['user_obj'] = user_obj
    elif slug == 'experience':
        result_data = ''
        page_title = _('Work experiences')
        experiences = WorkExperience.objects.filter(user=user_obj).order_by('-id')
        for item in experiences:
            result_data = "{}{}".format(result_data, render_to_string(
                "userprofile/include/experience-item.html",
                {
                    "data_item": item,
                }))
        context['new_form'] = WorkExperienceForm(request.POST)
        context['page_title'] = page_title
        context['result_data'] = result_data
    elif slug == 'education':
        page_title = _('Education')
        result_data = ''
        experiences = Education.objects.filter(user=user_obj).order_by('-id')
        for item in experiences:
            result_data = "{}{}".format(result_data, render_to_string(
                "userprofile/include/education-item.html",
                {
                    "data_item": item,
                }))

        context['new_form'] = EducationForm(request.POST)
        context['page_title'] = page_title
        context['result_data'] = result_data

    context['slug'] =  slug
    return render(request, 'userprofile/settings.html', context=context)




@login_required(login_url='base-user:login')
def experience_andeducation_create(request,slug):
    now = datetime.datetime.now()
    context = base_auth(req=request)
    user = request.user
    if slug == 'experience':
        form = WorkExperienceForm(request.POST or None)
        count = WorkExperience.objects.filter(user=user).count()
    else:
        form = EducationForm(request.POST or None)
        count = Education.objects.filter(user=user).count()
    if request.method == 'POST' and request.is_ajax() and slug in ['experience','education']:
        message_code = 0
        message_text = ''
        if count >= 100:
            message_text = _('You have 100 data. you can not add more')
        elif form.is_valid():
            form_val = form.save(commit=False)
            form_val.user = user
            form_val.save()
            message_code = 1
            message_text = _('Succesfully Added')
        else:
            message_text = str(form.errors)
        return JsonResponse(data={'code':message_code,'text':message_text})





@login_required(login_url='base-user:login')
def experience_andeducation_list(request,slug):
    now = datetime.datetime.now()
    context = base_auth(req=request)
    user_obj = request.user
    if request.method == 'POST' and request.is_ajax() and slug in ['experience','education']:
        message_code = 0

        result_data = ''
        if slug == 'experience':
            page_title = _('Work experiences')
            experiences = WorkExperience.objects.filter(user=user_obj).order_by('-id')
            for item in experiences:
                result_data = "{}{}".format(result_data, render_to_string(
                    "userprofile/include/experience-item.html",
                    {
                        "data_item": item,
                    }))
            context['result_data'] = result_data
            message_code = 1
        elif slug == 'education':
            page_title = _('Education')
            experiences = Education.objects.filter(user=user_obj).order_by('-id')
            for item in experiences:
                result_data = "{}{}".format(result_data, render_to_string(
                    "userprofile/include/education-item.html",
                    {
                        "data_item": item,
                    }))
            message_code = 1
        return JsonResponse(data={'code':message_code,'text':result_data})
    else:
        raise Http404



@login_required(login_url='base-user:login')
def friend_list(request,slug):
    now = datetime.datetime.now()
    context = base_auth(req=request)
    user = request.user
    search_form = MainSearchForm(request.POST or None)
    if slug == 'followers':
        page_title = 'İzləyicilər'
    elif slug == 'followings':
        page_title = 'İzləyənlər'
    else:
        raise Http404
    # return '\n'.join(url.format(year) for year in range(2000, 2016))
    if search_form.is_valid() and request.is_ajax():

        cleaned_data = search_form.cleaned_data
        section_parts = request.POST.getlist('section_parts')
        search_text = cleaned_data.get('search_text',None)
        user_type = 'user_to'
        if slug == 'followers':
            user_type = 'user_from'
            if search_text:
                user_items = FollowModel.objects.filter(user_to=user).filter(Q(user_from__first_name__icontains=search_text) | Q(user_from__last_name__icontains=search_text) | Q(user_from__email__icontains=search_text))
            else:
                user_items = FollowModel.objects.filter(user_to=user)
        elif slug == 'followings':
            user_type = 'user_to'
            if search_text:
                user_items = FollowModel.objects.filter(user_to__user_from=user).filter(Q(user_to__first_name__icontains=search_text) | Q(user_to__last_name__icontains=search_text) | Q(user_to__email__icontains=search_text))
            else:
                user_items = FollowModel.objects.filter(user_from=user)

        else:
            return False
        try:
            page_num = int(request.POST.get('page-num', 2))
        except:
            page_num = 0

        user_items = user_items[50 * (page_num - 1):50 * page_num]
        list_html = ''
        for item in user_items:
            list_html = "{}{}".format(list_html, render_to_string(
                "home/include/user-item.html",
                {
                    "data_item": item.user_from if user_type == 'user_from' else item.user_to,
                }))
        data = {
            'main_result':list_html,
        }
        return JsonResponse(data=data)
    context['search_form'] =  search_form
    context['page_title'] =  page_title
    # context['mac_address'] =  Package.objects.filter(status__in=['active','waiting']).count()
    return render(request, 'home/friend-list.html', context=context)




@login_required(login_url='base-user:login')
def users_list(request,):
    now = datetime.datetime.now()
    context = base_auth(req=request)
    user = request.user
    search_form = MainSearchForm(request.POST or None)

    # return '\n'.join(url.format(year) for year in range(2000, 2016))
    if search_form.is_valid() and request.method == 'POST':
        cleaned_data = search_form.cleaned_data
        if request.is_ajax():
            section_parts = request.POST.getlist('section_parts')
            user_types = request.POST.getlist('user_types')
            search_text = cleaned_data.get('search_text',None)
            user_items_gen = GUser.objects.exclude(id=user.id)
            if search_text:
                firstname = ''
                lastname = ''
                try:
                    firstname = search_text.split(' ')[0]
                except:
                    pass
                try:
                    lastname = search_text.split(' ')[1]
                except:
                    pass
                if firstname:
                    if lastname:
                        user_items = user_items_gen.filter(Q(Q(first_name__icontains=firstname, last_name__icontains=lastname) | Q(Q(first_name__icontains=lastname, last_name__icontains=firstname))))
                    else:
                        user_items = user_items_gen.filter(Q(first_name__icontains=firstname) | Q(last_name__icontains=firstname)).order_by('-date_joined')
                else:
                    user_items = user_items_gen.order_by('-date_joined')

            else:
                user_items = user_items_gen.order_by('-date_joined')

            if user_types:
                user_items = user_items.filter(usertype__in=user_types)
            try:
                page_num = int(request.POST.get('page-num', 2))
            except:
                page_num = 0

            user_items = user_items[50 * (page_num - 1):50 * page_num]
            list_html = ''
            for item in user_items:
                list_html = "{}{}".format(list_html, render_to_string(
                    "home/include/user-item.html",
                    {
                        "data_item": item,
                    }))
            data = {
                'main_result':list_html,
            }
            return JsonResponse(data=data)
        else:
            if cleaned_data.get('search_text',None):
                search_form = MainSearchForm(request.POST or None,initial={'search_text':cleaned_data.get('search_text',None)})
    context['search_form'] =  search_form
    context['user_type_choices'] =  USERTYPESSEARCH
    context['sections'] =  Section.objects.all()
    # context['page_title'] =  page_title
    # context['mac_address'] =  Package.objects.filter(status__in=['active','waiting']).count()
    return render(request, 'home/user-list.html', context=context)





@login_required(login_url='base-user:login')
def message_notification_show(request,slug):
    user = request.user
    now = datetime.datetime.now()
    if request.method == 'POST' and request.is_ajax():
        count = 0
        data_html = ''
        if slug == 'notifications':
            notifications_list = Notification.objects.filter(to_user=user,is_view=False).order_by('-created_date')
            count = notifications_list.count()
            for item in notifications_list[:4]:
                data_html = "{}{}".format(data_html, render_to_string(
                    "home/include/notifications/notification-item-general.html",
                    {
                        "item": item,
                    }))
        if slug == 'messages':
            threads_list = Thread.objects.by_user(user=user).filter(last_message__user=user,last_message__seen=False).order_by('-updated_date')
            count = threads_list.count()
            for item in threads_list[:4]:
                data_html = "{}{}".format(data_html, render_to_string(
                    "home/include/notifications/message-item-general.html",
                    {
                        "item": item,
                        "to_user": item.second if item.first == request.user else item.first
                    }))
        data = {
            'count': count,
            'result_html': data_html,
        }
        return JsonResponse(data=data)

@login_required(login_url='base-user:login')
def notifications_list(request,):
    now = datetime.datetime.now()
    context = base_auth(req=request)
    user = request.user
    search_form = MainSearchForm(request.POST or None)

    # return '\n'.join(url.format(year) for year in range(2000, 2016))
    if search_form.is_valid() and request.is_ajax():

        cleaned_data = search_form.cleaned_data
        search_text = cleaned_data.get('search_text',None)
        notifications_list = Notification.objects.filter(to_user=user).order_by('-created_date')
        if search_text:
            notifications_list = notifications_list.filter(Q(title__icontains=search_text) | Q(content__icontains=search_text))
        try:
            page_num = int(request.POST.get('page-num', 2))
        except:
            page_num = 0

        notifications_list = notifications_list[50 * (page_num - 1):50 * page_num]
        list_html = ''
        for item in notifications_list:
            list_html = "{}{}".format(list_html, render_to_string(
                "home/include/notifications/list-item.html",
                {
                    "item": item,
                }))
        data = {
            'main_result':list_html,
        }
        return JsonResponse(data=data)
    context['search_form'] =  search_form
    return render(request, 'home/notifications.html', context=context)



@login_required(login_url='base-user:login')
def notification_remove(request,id):
    now = datetime.datetime.now()
    context = base_auth(req=request)
    user = request.user
    current_user_ch = False
    if request.method == 'POST' and request.is_ajax():
        try:
            post_item = Notification.objects.get(id=id,to_user=user)
            post_item.delete()
            code = 1
        except:
            code = 0
        data = {
            'code' : code,
        }
        return JsonResponse(data)
    else:
        raise Http404


@login_required(login_url='base-user:login')
def event_list(request):
    now = datetime.datetime.now()
    context = base_auth(req=request)
    user = request.user
    search_form = MainSearchForm(request.POST or None)
    page_title = 'İzləyicilər'
    # return '\n'.join(url.format(year) for year in range(2000, 2016))
    if search_form.is_valid() and request.is_ajax():

        cleaned_data = search_form.cleaned_data
        search_text = cleaned_data.get('search_text',None)
        event_items = Event.objects.all()
        if search_text:
            event_items = event_items.filter(Q(title__icontains=search_text)|Q(content__icontains=search_text))
        try:
            page_num = int(request.POST.get('page-num', 2))
        except:
            page_num = 0

        event_items = event_items[50 * (page_num - 1):50 * page_num]
        list_html = ''
        list_html = ''
        for item in event_items:
            list_html = "{}{}".format(list_html, render_to_string(
                "home/include/events/event-item.html",
                {
                    "data_item": item,
                }))
        data = {
            'main_result':list_html,
        }
        return JsonResponse(data=data)
    context['search_form'] =  search_form
    context['page_title'] =  page_title
    # context['mac_address'] =  Package.objects.filter(status__in=['active','waiting']).count()
    return render(request, 'home/friend-list.html', context=context)






@login_required(login_url='base-user:login')
def blognews_list(request):
    now = datetime.datetime.now()
    context = base_auth(req=request)
    user = request.user
    search_form = MainSearchForm(request.POST or None)
    page_title = 'İzləyicilər'
    # return '\n'.join(url.format(year) for year in range(2000, 2016))
    if search_form.is_valid() and request.is_ajax():

        cleaned_data = search_form.cleaned_data
        search_text = cleaned_data.get('search_text',None)
        event_items = Event.objects.all()
        if search_text:
            event_items = event_items.filter(Q(title__icontains=search_text)|Q(content__icontains=search_text))
        try:
            page_num = int(request.POST.get('page-num', 2))
        except:
            page_num = 0

        event_items = event_items[50 * (page_num - 1):50 * page_num]
        list_html = ''
        list_html = ''
        for item in event_items:
            list_html = "{}{}".format(list_html, render_to_string(
                "home/include/events/event-item.html",
                {
                    "data_item": item,
                }))
        data = {
            'main_result':list_html,
        }
        return JsonResponse(data=data)
    context['search_form'] =  search_form
    context['page_title'] =  page_title
    # context['mac_address'] =  Package.objects.filter(status__in=['active','waiting']).count()
    return render(request, 'home/friend-list.html', context=context)




@login_required(login_url='base-user:login')
def tutorials(request):
    now = datetime.datetime.now()
    context = base_auth(req=request)

    # return '\n'.join(url.format(year) for year in range(2000, 2016))
    # context['mac_address'] =  ':'.join(("%012X" % mac)[i:i+2] for i in range(0, 12, 2))
    # context['mac_address'] =  Package.objects.filter(status__in=['active','waiting']).count()
    return render(request, 'tutorials/main.html', context=context)









@login_required(login_url='base-user:login')
def all_sections(request):
    now = datetime.datetime.now()
    context = base_auth(req=request)

    # return '\n'.join(url.format(year) for year in range(2000, 2016))
    # context['mac_address'] =  ':'.join(("%012X" % mac)[i:i+2] for i in range(0, 12, 2))
    context['all_sections'] =  Section.objects.all()
    return render(request, 'tutorials/main.html', context=context)




@login_required(login_url='base-user:login')
def section_item(request,id):
    now = datetime.datetime.now()
    context = base_auth(req=request)
    section_obj = get_object_or_404(Section,id=id)

    # return '\n'.join(url.format(year) for year in range(2000, 2016))
    # context['mac_address'] =  ':'.join(("%012X" % mac)[i:i+2] for i in range(0, 12, 2))
    context['section_obj'] =  section_obj
    context['section_parts'] =  SectionPart.objects.filter(section=section_obj)

    search_form =  MainSearchForm(request.POST or None)
    if search_form.is_valid() and request.is_ajax():
        cleaned_data = search_form.cleaned_data
        section_parts = request.POST.getlist('section_parts')
        search_text = cleaned_data.get('search_text',None)
        article_items = Article.objects.filter(section_part__section=section_obj)
        if section_parts:
            try:
                section_parts = list(section_parts)
                article_items = article_items.filter(section_part_id__in=section_parts)
            except:
                pass
        if search_text:
            article_items = article_items.filter(Q(title__icontains=search_text)|Q(content__icontains=search_text))
        try:
            page_num = int(request.POST.get('page-num', 2))
        except:
            page_num = 0

        article_items = article_items[50 * (page_num - 1):50 * page_num]
        list_html = ''
        for item in article_items:
            list_html = "{}{}".format(list_html, render_to_string(
                "home/include/list-item.html",
                {
                    "data_item": item,
                }))
        data = {
            'main_result':list_html,
        }
        return JsonResponse(data=data)

    context['search_form'] =  MainSearchForm(request.POST or None)

    return render(request, 'tutorials/main-item.html', context=context)




@login_required(login_url='base-user:login')
def article_item(request,id):
    now = datetime.datetime.now()
    context = base_auth(req=request)
    article_obj = get_object_or_404(Article,id=id)
    context['article_obj'] =  article_obj

    return render(request, 'tutorials/article-item.html', context=context)







@login_required(login_url='base-user:login')
def projects_list(request):
    now = datetime.datetime.now()
    context = base_auth(req=request)

    # return '\n'.join(url.format(year) for year in range(2000, 2016))
    # context['mac_address'] =  ':'.join(("%012X" % mac)[i:i+2] for i in range(0, 12, 2))
    context['sections'] =  Section.objects.filter()
    random_projects = Project.objects.filter().order_by('?')[:10]
    context['random_projects'] =  random_projects

    search_form =  MainSearchForm(request.POST or None)
    if search_form.is_valid() and request.is_ajax():
        cleaned_data = search_form.cleaned_data
        section_parts = request.POST.getlist('section_parts')
        search_text = cleaned_data.get('search_text',None)
        project_items = Project.objects.filter()
        if section_parts:
            try:
                section_parts = list(section_parts)
                project_items = project_items.filter(section_id__in=section_parts)
            except:
                pass
        if search_text:
            project_items = project_items.filter(Q(title__icontains=search_text)|Q(content__icontains=search_text))
        try:
            page_num = int(request.POST.get('page-num', 2))
        except:
            page_num = 0

        project_items = project_items[50 * (page_num - 1):50 * page_num]
        list_html = ''
        for item in project_items:
            list_html = "{}{}".format(list_html, render_to_string(
                "datas/include/item.html",
                {
                    "data_item": item,
                }))
        data = {
            'main_result':list_html,
        }
        return JsonResponse(data=data)

    context['search_form'] =  MainSearchForm(request.POST or None)

    return render(request, 'datas/project-list.html', context=context)







@login_required(login_url='base-user:login')
def projects_create(request):
    now = datetime.datetime.now()
    context = base_auth(req=request)
    user = request.user
    form = ProjectModelForm(request.POST or None, request.FILES or None,)
    if request.method == 'POST':
        if form.is_valid():
            form_val = form.save(commit=False)
            form_val.author = user
            form_val.save()
            messages.success(request, _('Succesfully Added'))
            form = ProjectModelForm()
        # else:
        #     return HttpResponse(employee_form.errors)
    context['form'] = form

    return render(request, 'datas/project-form.html', context=context)



@login_required(login_url='base-user:login')
def projects_edit(request,id):
    user = request.user
    data_obj = get_object_or_404(Project,id=id)
    context = base_auth(req=request)
    if request.method == "POST":
        form = ProjectModelForm(request.POST or None, request.FILES or None, instance=data_obj)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()

            messages.success(request, _('Succesfully changed'))
    else:
        form = ProjectModelForm(instance=data_obj)
    context['form'] = form
    context['data_obj'] = data_obj

    return render(request, 'datas/project-form.html', context=context)



@login_required(login_url='base-user:login')
def projects_view(request,id):
    user = request.user
    data_obj = get_object_or_404(Project,id=id)
    context = base_auth(req=request)
    context['data_obj'] = data_obj

    return render(request, 'datas/project-view.html', context=context)








@login_required(login_url='base-user:login')
def get_photo_profile(request,type):
    now = datetime.datetime.now()
    user = request.user
    if request.method == 'POST' and request.is_ajax():
        main_html = ''
        objs = ProfilePhoto.objects.filter(type=type,user=user)
        if type == 'profile':
            for data_item in objs:
                main_html = "{}{}".format(main_html,render_to_string(
                    "userprofile/include/profile-photo-item.html",{
                        "data_item":data_item
                    }
                ))
        elif type == 'cover':
            for data_item in objs:
                main_html = "{}{}".format(main_html,render_to_string(
                    "userprofile/include/cover-photo-item.html",{
                        "data_item":data_item
                    }
                ))
        data = {
            'main-data':main_html,
        }
        return JsonResponse(data=data)
    else:
        raise Http404






@login_required(login_url='base-user:login')
def events_list(request):
    now = datetime.datetime.now()
    context = base_auth(req=request)

    # return '\n'.join(url.format(year) for year in range(2000, 2016))
    # context['mac_address'] =  ':'.join(("%012X" % mac)[i:i+2] for i in range(0, 12, 2))
    context['sections'] =  Section.objects.filter()
    random_events = Event.objects.filter().order_by('?')[:10]
    context['random_events'] =  random_events

    search_form =  MainSearchForm(request.POST or None)
    if search_form.is_valid() and request.is_ajax():
        cleaned_data = search_form.cleaned_data
        section_parts = request.POST.getlist('section_parts')
        search_text = cleaned_data.get('search_text',None)
        event_items = Event.objects.filter()
        if section_parts:
            try:
                section_parts = list(section_parts)
                event_items = event_items.filter(section_id__in=section_parts)
            except:
                pass
        if search_text:
            event_items = event_items.filter(Q(title__icontains=search_text)|Q(content__icontains=search_text))
        try:
            page_num = int(request.POST.get('page-num', 2))
        except:
            page_num = 0

        event_items = event_items[50 * (page_num - 1):50 * page_num]
        list_html = ''
        for item in event_items:
            list_html = "{}{}".format(list_html, render_to_string(
                "datas/include/item.html",
                {
                    "data_item": item,
                }))
        data = {
            'main_result':list_html,
        }
        return JsonResponse(data=data)

    context['search_form'] =  MainSearchForm(request.POST or None)

    return render(request, 'datas/event/list.html', context=context)
@login_required(login_url='base-user:login')
def events_view(request,id):
    data_obj = get_object_or_404(Event,id=id)
    context = base_auth(req=request)
    context['data_obj'] = data_obj

    return render(request, 'datas/event/detail.html', context=context)




@login_required(login_url='base-user:login')
def news_list(request):
    now = datetime.datetime.now()
    context = base_auth(req=request)

    # return '\n'.join(url.format(year) for year in range(2000, 2016))
    # context['mac_address'] =  ':'.join(("%012X" % mac)[i:i+2] for i in range(0, 12, 2))
    context['sections'] =  Section.objects.filter()
    random_news = Event.objects.filter().order_by('?')[:10]
    context['random_news'] =  random_news

    search_form =  MainSearchForm(request.POST or None)
    if search_form.is_valid() and request.is_ajax():
        cleaned_data = search_form.cleaned_data
        section_parts = request.POST.getlist('section_parts')
        search_text = cleaned_data.get('search_text',None)
        news_items = Event.objects.filter()
        if section_parts:
            try:
                section_parts = list(section_parts)
                news_items = news_items.filter(section_id__in=section_parts)
            except:
                pass
        if search_text:
            news_items = news_items.filter(Q(title__icontains=search_text)|Q(content__icontains=search_text))
        try:
            page_num = int(request.POST.get('page-num', 2))
        except:
            page_num = 0

        news_items = news_items[50 * (page_num - 1):50 * page_num]
        list_html = ''
        for item in news_items:
            list_html = "{}{}".format(list_html, render_to_string(
                "datas/include/item.html",
                {
                    "data_item": item,
                }))
        data = {
            'main_result':list_html,
        }
        return JsonResponse(data=data)

    context['search_form'] =  MainSearchForm(request.POST or None)

    return render(request, 'datas/news/list.html', context=context)
@login_required(login_url='base-user:login')
def news_view(request,id):
    data_obj = get_object_or_404(Event,id=id)
    context = base_auth(req=request)
    context['data_obj'] = data_obj

    return render(request, 'datas/news/detail.html', context=context)


