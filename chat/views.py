import datetime

from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.http import Http404, HttpResponseForbidden, JsonResponse
from django.shortcuts import render
from django.template.loader import render_to_string
from django.urls import reverse
from django.views.generic.edit import FormMixin

from django.views.generic import DetailView, ListView
GUser = get_user_model()

from general.views import base_auth
from .forms import ComposeForm, MainSearchForm, MessageForm
from .models import Thread, ChatMessage


from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import Http404, HttpResponseForbidden
from django.shortcuts import render
from django.urls import reverse
from django.views.generic.edit import FormMixin

from django.views.generic import DetailView, ListView

from .forms import ComposeForm
from .models import Thread, ChatMessage


@login_required(login_url='base-user:login')
def inbox_view(request):
    now = datetime.datetime.now()
    context = base_auth(req=request)

    threads =  Thread.objects.by_user(request.user).exclude(last_message=None)
    # return '\n'.join(url.format(year) for year in range(2000, 2016))
    context['threads'] =  threads
    context['message_box_form'] =  MainSearchForm(request.POST or None)
    return render(request, 'chat/inbox.html', context=context)




@login_required(login_url='base-user:login')
def message_box_list(request,):
    user = request.user
    search_form = MainSearchForm(request.POST or None)

    # return '\n'.join(url.format(year) for year in range(2000, 2016))
    if search_form.is_valid() and request.is_ajax():

        cleaned_data = search_form.cleaned_data
        search_text = cleaned_data.get('search_text',None)
        threads_items_gen = Thread.objects.by_user(request.user).exclude(last_message=None)
        firstname = ''
        lastname = ''
        try:
            firstname = search_text.split(' ')[0]
        except:
            pass
        try:
            lastname = search_text.split(' ')[1]
        except:
            pass
        print("{} - {}".format(firstname,lastname))
        # threads_items = []
        if search_text:
            if firstname:
                if lastname:
                    threads_items = threads_items_gen.filter(Q(Q(first__first_name__icontains=firstname, first__last_name__icontains=lastname) | Q(second__first_name__icontains=firstname, second__last_name__icontains=lastname)) | Q(Q(first__first_name__icontains=lastname, first__last_name__icontains=firstname) | Q(second__first_name__icontains=lastname, second__last_name__icontains=firstname)))
                    # threads_items = threads_items_gen.filter().order_by('-updated_date')
                else:
                    threads_items = threads_items_gen.filter(Q(first__first_name__icontains=firstname) | Q(first__last_name__icontains=firstname) | Q(second__first_name__icontains=firstname) | Q(second__last_name__icontains=firstname)).order_by('-updated_date')
            else:
                threads_items = threads_items_gen.order_by('-updated_date')

        else:
            threads_items = threads_items_gen.order_by('-updated_date')

        try:
            page_num = int(request.POST.get('page-num-message-box', 2))
        except:
            page_num = 0

        threads_items = threads_items[20 * (page_num - 1):20 * page_num]
        list_html = ''
        for item in threads_items:
            is_ssen = True
            if item.last_message:
                if item.last_message.user != user and item.last_message.seen  == False:
                    is_ssen = False
            list_html = "{}{}".format(list_html, render_to_string(
                "chat/include/message-box-item.html",
                {
                    "data_item": item,
                    "is_seen": is_ssen,
                    "to_user": item.second if item.first == request.user else item.first
                }))
        data = {
            'main_result':list_html,
        }

        return JsonResponse(data)



class ThreadView(LoginRequiredMixin, FormMixin, DetailView):
    template_name = 'chat/inbox.html'
    form_class = ComposeForm
    success_url = './'

    def get_queryset(self):
        return Thread.objects.by_user(self.request.user).exclude(last_message=None)

    def get_object(self):
        other_uuid_id  = self.kwargs.get("uuid_id")
        obj, created    = Thread.objects.get_or_new(self.request.user, other_uuid_id)
        if obj == None:
            raise Http404
        return obj

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = self.get_form()
        context['message_box_form'] =  MainSearchForm(self.request.POST or None)
        context['message_form'] =  ComposeForm(self.request.POST or None)
        context.update(base_auth(req=self.request))
        return context

    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return HttpResponseForbidden()
        self.object = self.get_object()
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        thread = self.get_object()
        user = self.request.user
        message = form.cleaned_data.get("message")
        ChatMessage.objects.create(user=user, thread=thread, message=message)
        return JsonResponse(data={'code':1})



@login_required(login_url='base-user:login')
def comment_share(request,uuid_id):
    now = datetime.datetime.now()
    context = base_auth(req=request)
    user = request.user
    form = ComposeForm(request.POST or None,request.FILES or None)
    if request.method == 'POST' and  request.is_ajax():
        message_code = 0
        message = 0
        if form.is_valid():
            cleaned_data = form.cleaned_data
            text = cleaned_data.get('text',None)
            to_user = GUser.object.get(uuid_id=uuid_id)
            data_item = ChatMessage(content=text,user=to_user)
            data_item.save()
            message_code = 1
            message = 'Uğurlu'
        else:
            message = " , ".join([v[0].__unicode__() for k, v in form.errors.items()])

        data = {
            'code':message_code,
            'result':message,
        }
        return JsonResponse(data=data)

    else:
        raise Http404



def get_comments(request,uuid_id):
    now = datetime.datetime.now()
    user = request.user
    context = base_auth(req=request)

    if request.method == 'POST' and request.is_ajax():
        try:
            page_num = int(request.POST.get('post-page-num', 1))
        except:
            page_num = 0
        try:
            loaded_post_count = int(request.POST.get('loaded-post-count', 1))
        except:
            loaded_post_count = 0
        obj, created    = Thread.objects.get_or_new(request.user, uuid_id)
        if obj == None:
            raise Http404
        posts = ChatMessage.objects.filter(thread=obj).order_by('-id')

        posts = posts[50 * (page_num - 1) + loaded_post_count:50 * page_num + loaded_post_count]
        list_html = ''
        last_item  = None
        for_i = 0
        for item in posts:
            if for_i == 0 and page_num == 1:
                last_item = item
            for_i +=1
            list_html = "{}{}".format(render_to_string(
                "chat/include/message-text-item.html",
                {
                    "chat": item,
                    "base_profile": request.user,
                }),list_html)
        data = {
            'main_result':list_html,
            'date_time':last_item.id if last_item else 0,
            # 'date_time':posts.first().timestamp.strftime("%d/%m/%Y, %H:%M:%S") if posts else '',
        }
        return JsonResponse(data=data)
    else:
        raise Http404





def get_comments_renew(request,uuid_id):
    now = datetime.datetime.now()
    user = request.user

    if request.method == 'POST' and request.is_ajax():
        list_html = ''
        date_time = ''
        extra_count = 0
        try:
            try:
                date_time = int(request.POST.get('date_time', 1))
            except:
                date_time = -1
            obj, created    = Thread.objects.get_or_new(user, uuid_id)
            if obj == None:
                raise Http404
            posts = ChatMessage.objects.filter(thread=obj).filter(id__gt=date_time).order_by('-id')
            last_item = None
            for_i = 0
            if posts:
                code = 1
                extra_count = posts.count()
                for item in posts:
                    if for_i == 0:
                        last_item = item
                    for_i += 1
                    list_html = "{}{}".format(render_to_string(
                        "chat/include/message-text-item.html",
                        {
                            "chat": item,
                            "base_profile": request.user,
                        }),list_html)
                date_time = last_item.id
                # date_time = posts.first().timestamp.strftime("%d/%m/%Y, %H:%M:%S") if posts else ''
            else:
                code = 2
        except:
            code = 0
        data = {
            'main_result':list_html,
            'date_time':date_time,
            'extra_count':extra_count,
            'code':code,
        }
        return JsonResponse(data=data)
    else:
        raise Http404






