from django import forms
from django.utils.translation import ugettext as _


class ComposeForm(forms.Form):
    message = forms.CharField(max_length=5000,label=_('Mesaj'), required=True, widget=forms.Textarea(attrs={ 'autocomplete': 'off','class': 'form-control','rows': '3', }))



class MainSearchForm(forms.Form):
    search_text = forms.CharField(max_length=255,label=_('Axtar'), required=False, widget=forms.TextInput(
        attrs={'placeholder': _('Axtar'), 'autocomplete': 'off',
               'class': 'form-control', }))



class MessageForm(forms.Form):
    message_text = forms.CharField(max_length=5000,label=_('Mesaj'), required=True, widget=forms.Textarea(attrs={ 'autocomplete': 'off','class': 'form-control','rows': '3', }))
