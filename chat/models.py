import datetime

from django.conf import settings
from django.db import models
from django.db.models import Q
from django.db.models.signals import post_save

# from .utils import broadcast_msg_to_chat, trigger_welcome_message


class ThreadManager(models.Manager):
    def by_user(self, user):
        qlookup = Q(first=user) | Q(second=user)
        qlookup2 = Q(first=user) & Q(second=user)
        qs = self.get_queryset().filter(qlookup).exclude(qlookup2).distinct()
        return qs

    def get_or_new(self, user, other_uuid_id):  # get_or_create
        uuid_id = user.uuid_id
        if uuid_id == other_uuid_id:
            return None
        qlookup1 = Q(first__uuid_id=uuid_id) & Q(second__uuid_id=other_uuid_id)
        qlookup2 = Q(first__uuid_id=other_uuid_id) & Q(second__uuid_id=uuid_id)
        qs = self.get_queryset().filter(qlookup1 | qlookup2).distinct()
        if qs.count() == 1:
            return qs.first(), False
        elif qs.count() > 1:
            return qs.order_by('timestamp').first(), False
        else:
            Klass = user.__class__
            user2 = Klass.objects.get(uuid_id=other_uuid_id)
            if user != user2:
                obj = self.model(
                    first=user,
                    second=user2
                )
                obj.save()
                return obj, True
            return None, False


class Thread(models.Model):
    first = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='chat_thread_first')
    second = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='chat_thread_second')
    updated = models.DateTimeField(auto_now=True)
    last_message = models.ForeignKey('ChatMessage',blank=True,null=True, on_delete=models.SET_NULL,related_name='last_message')
    updated_date = models.DateTimeField(default=datetime.datetime.now())
    timestamp = models.DateTimeField(auto_now_add=True)

    objects = ThreadManager()

    @property
    def room_group_name(self):
        return 'chat_{}'.format(self.id)

    # def broadcast(self, msg=None):
    #     if msg is not None:
    #         broadcast_msg_to_chat(msg, group_name=self.room_group_name, user='admin')
    #         return True
    #     return False


def new_user_receiver(sender, instance, created, *args, **kargs):
    if created:
        # UserKlass = instance.__class__
        # my_admin_user = UserKlass.objects.get(id=1)
        # obj, created = Thread.objects.get_or_new(my_admin_user, instance.username)
        # obj.broadcast(msg='Hello and welcome')

        sender_id = 1  # admin user, main sender
        receiver_id = instance.id
        # trigger_welcome_message(sender_id, receiver_id)


# post_save.connect(new_user_receiver, sender=settings.AUTH_USER_MODEL)


class ChatMessage(models.Model):
    thread = models.ForeignKey(Thread, null=True, blank=True, on_delete=models.SET_NULL)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name='sender', on_delete=models.CASCADE)
    seen = models.BooleanField(default=False)
    message = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)

def new_message_send(sender, instance, created, *args, **kargs):
    if created:
        instance.thread.last_message = instance
        instance.thread.updated_date = instance.timestamp
        instance.thread.save()

post_save.connect(new_message_send, sender=ChatMessage)