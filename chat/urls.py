from django.urls import path, re_path


from .views import ThreadView, inbox_view, message_box_list, get_comments, get_comments_renew

app_name = 'chat'
urlpatterns = [
    path("", inbox_view, name='inbox'),
    path("message-box-list", message_box_list, name='message-box-list'),
    path("get-comments-list/<uuid_id>",get_comments,name='get-comments-list'),
    path("get-comments-list-renew/<uuid_id>",get_comments_renew,name='get-comments-renew-list'),
    re_path(r"^(?P<uuid_id>[\w.@+-]+)", ThreadView.as_view(),name='message-box-detail'),
    # re_path(r"^get-comments-list/(?P<uuid_id>[\w.@+-]+)", get_comments,name='get-comments-list'),
]