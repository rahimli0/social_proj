# Generated by Django 2.2 on 2020-01-13 06:01

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('chat', '0009_auto_20200112_1815'),
    ]

    operations = [
        migrations.AlterField(
            model_name='thread',
            name='updated_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 1, 13, 6, 1, 11, 43634)),
        ),
    ]
