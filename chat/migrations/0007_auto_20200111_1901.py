# Generated by Django 2.2 on 2020-01-11 15:01

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('chat', '0006_auto_20200110_1654'),
    ]

    operations = [
        migrations.AlterField(
            model_name='thread',
            name='updated_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 1, 11, 19, 1, 16, 190626)),
        ),
    ]
