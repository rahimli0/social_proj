# Generated by Django 2.2 on 2020-01-11 15:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0003_auto_20200110_1603'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sharedpost',
            name='content',
            field=models.TextField(max_length=2000, verbose_name='Content'),
        ),
    ]
