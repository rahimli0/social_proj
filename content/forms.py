from django import forms

from django.utils.translation import ugettext_lazy as _
from content.models import Project


class ProjectModelForm(forms.ModelForm):
    content = forms.CharField(label=_('Mətni'), required=False, widget=forms.Textarea(attrs={ 'autocomplete': 'off','class': 'form-control', }))
    class Meta:
        model = Project
        fields = ('section', 'image', 'title', 'budget', 'short_title','content',)
    widgets = {
        'title': forms.TextInput(attrs={'class': 'form-control','placeholder': _("Title") }),
        'section': forms.Select(attrs={'class': 'form-control selectpicker','placeholder': _("Section") }),
        'image': forms.FileInput(attrs={'class': 'form-control'}),
        'short_title': forms.TextInput(attrs={'class': 'form-control', 'placeholder': _("Short title")}),
        # 'content': forms.Textarea(attrs={'class': 'form-control','rows':5, }),
        'budget': forms.NumberInput(attrs={'class': 'form-control','placeholder': _("Budget") }),
    }
