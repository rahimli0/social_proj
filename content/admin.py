from django.contrib import admin

# Register your models here.

from content.models import *


@admin.register(Section, SectionPart,Article,Event,BlogNews,Education,WorkExperience,ProfilePhoto,MainSystemInfo,StaticPage,SharedPost,Notification)
class PersonAdmin(admin.ModelAdmin):
    pass

# @admin.register(MainSystemInfo,StaticPage)
# class TranslatableDataAdmin(TranslatableAdmin):
#     pass