from ckeditor_uploader.fields import RichTextUploadingField
from django.conf import settings
from django.db import models

# Create your models here.

from general.functions import path_and_rename
from django.utils.translation import ugettext as _




class ProfilePhoto(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,related_name="+",on_delete=models.CASCADE)
    photo = models.ImageField(upload_to=path_and_rename, null=True, blank=True)
    type = models.CharField(max_length=255, default='profile',null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.user.get_full_name()


# Create your models here.
class MainSystemInfo(models.Model):
    image = models.ImageField(upload_to=path_and_rename, null=True, blank=True)
    logo_white = models.ImageField(upload_to=path_and_rename, null=True, blank=True)
    logo_colored = models.ImageField(upload_to=path_and_rename, null=True, blank=True)
    site_name = models.CharField("Site name",max_length=255)
    site_slogan = models.CharField("Site slogan",max_length=255)
    site_url = models.URLField("Url",max_length=255)

    short_title = models.CharField("Uzun slogan",max_length=255,blank=True,null=True,default='')
    short_desc = models.CharField("Qısa təsvir",max_length=255,blank=True,null=True,default='')
    desc = RichTextUploadingField("Təsvir",config_name='awesome_ckeditor',blank=True,null=True,default='')

    created_date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.site_name


class Education(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,related_name="+",on_delete=models.CASCADE)
    title = models.CharField(_("Başlıq"),max_length=255)
    university = models.CharField(_("University"),max_length=255)
    start_date = models.DateField(blank=True,null=True)
    end_date = models.DateField(blank=True,null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.title


class WorkExperience(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,related_name="+",on_delete=models.CASCADE)
    title = models.CharField("Site name",max_length=255)
    company = models.CharField("Site name",max_length=255)
    start_date = models.DateField(blank=True,null=True)
    end_date = models.DateField(blank=True,null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.title




class SharedPost(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,related_name="+",on_delete=models.CASCADE)
    content = models.TextField("Content",max_length=5000)
    image = models.ImageField(upload_to=path_and_rename, null=True, blank=True)
    liked_count = models.IntegerField(default=0)
    saved_count = models.IntegerField(default=0)
    created_date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return "{}-{}-{}".format(self.created_date.day,self.created_date.month,self.created_date.year)


class SharedPostLike(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,related_name="+",on_delete=models.CASCADE)
    post = models.ForeignKey('SharedPost',related_name="+",on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)
    class Meta:
        unique_together = ('user', 'post',)

class SharedPostSaved(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,related_name="+",on_delete=models.CASCADE)
    post = models.ForeignKey('SharedPost',related_name="+",on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)
    class Meta:
        unique_together = ('user', 'post',)


class FollowModel(models.Model):
    user_to = models.ForeignKey(settings.AUTH_USER_MODEL,related_name="+",on_delete=models.CASCADE)
    user_from = models.ForeignKey(settings.AUTH_USER_MODEL,related_name="+",on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)
    class Meta:
        unique_together = ('user_to', 'user_from',)


class Section(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,related_name="+",on_delete=models.CASCADE)
    title = models.CharField("Başlıq",max_length=255,blank=True,null=True)
    image = models.ImageField(upload_to=path_and_rename, null=True, blank=True)
    content = RichTextUploadingField(config_name='awesome_ckeditor',blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.title


# Create your models here.
class SectionPart(models.Model):
    section = models.ForeignKey('Section',related_name="+",on_delete=models.CASCADE)
    author = models.ForeignKey(settings.AUTH_USER_MODEL,related_name="+",on_delete=models.CASCADE)
    title = models.CharField("Başlıq",max_length=255,blank=True,null=True)
    image = models.ImageField(upload_to=path_and_rename, null=True, blank=True)
    content = RichTextUploadingField(config_name='awesome_ckeditor',blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.title


# Create your models here.
class Article(models.Model):
    section_part = models.ForeignKey('SectionPart',related_name="+",on_delete=models.CASCADE)
    author = models.ForeignKey(settings.AUTH_USER_MODEL,related_name="+",on_delete=models.CASCADE)
    image = models.ImageField(upload_to=path_and_rename, null=True, blank=True)
    title = models.CharField("Başlıq",max_length=255)
    short_title = models.CharField("Qısa təsvir",max_length=255,blank=True,null=True,default='')
    content = RichTextUploadingField(config_name='awesome_ckeditor',blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.title





# Create your models here.
class Project(models.Model):
    section = models.ForeignKey('Section',related_name="+",on_delete=models.CASCADE)
    author = models.ForeignKey(settings.AUTH_USER_MODEL,related_name="+",on_delete=models.CASCADE)
    image = models.ImageField(upload_to=path_and_rename)
    title = models.CharField("Başlıq",max_length=255)
    budget = models.DecimalField(max_digits=19,decimal_places=2,)
    short_title = models.CharField("Qısa təsvir",max_length=255,blank=True,null=True,default='')
    content = RichTextUploadingField(config_name='awesome_ckeditor',blank=True,null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.title


# Create your models here.
class Event(models.Model):
    type = models.CharField("Type",max_length=255,default='event')
    author = models.ForeignKey(settings.AUTH_USER_MODEL,related_name="+",on_delete=models.CASCADE)
    image = models.ImageField(upload_to=path_and_rename, null=True, blank=True)
    title = models.CharField("Başlıq",max_length=255)
    short_title = models.CharField("Qısa təsvir",max_length=255,blank=True,null=True,default='')
    content = RichTextUploadingField(config_name='awesome_ckeditor',blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.title

class BlogNews(models.Model):
    section = models.ForeignKey('Section',related_name="+",on_delete=models.CASCADE)
    type = models.CharField("Type",max_length=255,default='event')
    author = models.ForeignKey(settings.AUTH_USER_MODEL,related_name="+",on_delete=models.CASCADE)
    image = models.ImageField(upload_to=path_and_rename, null=True, blank=True)
    title = models.CharField("Başlıq",max_length=255)
    short_title = models.CharField("Qısa təsvir",max_length=255,blank=True,null=True,default='')
    content = RichTextUploadingField(config_name='awesome_ckeditor',blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.title



StaticPageChoices = (
    ('about', _('About')),
    ('privacy-policy', _('Privacy and policy')),
    ('terms-and-conditions', _('Terms and Conditions')),
    # ('about', _('About')),
)


class StaticPage(models.Model):
    key_page = models.CharField("Key",choices=StaticPageChoices,max_length=255,blank=True,null=True,unique=True)
    icon = models.CharField(_("Icon"),max_length=255)
    image = models.ImageField(upload_to=path_and_rename, null=True, blank=True)
    title = models.CharField(_('Başlıq'),max_length=255,blank=True,null=True)
    short_text = models.CharField(_('Short text'),max_length=500,blank=True,null=True)
    content = RichTextUploadingField(config_name='awesome_ckeditor',blank=True,null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.key_page



class Notification(models.Model):
    to_user = models.ForeignKey(settings.AUTH_USER_MODEL,related_name="+",on_delete=models.CASCADE)
    title = models.CharField(_('Başlıq'),max_length=255,blank=True,null=True)
    is_read = models.BooleanField(default=False)
    is_view = models.BooleanField(default=False)
    content = RichTextUploadingField(config_name='awesome_ckeditor',blank=True,null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.title

