from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin, UserManager
from django.db import models
from django.utils import timezone
from django.conf import settings
from django.core import validators
from django.utils.translation import ugettext_lazy as _
# from core.functions import path_and_rename
from datetime import datetime as d
import calendar
from django.db.models import Q
# from work.common import *
# My custom tools import



# Create your models here.
from image_cropping import ImageRatioField

from base_user.tools.common import USERTYPES, USERTYPESDICT, slugify
from content.models import ProfilePhoto
from general.functions import path_and_rename

USER_MODEL = settings.AUTH_USER_MODEL
import random
import uuid
class Image(models.Model):
    image_field = models.ImageField(upload_to='image/')
# Customize User model
class MyUser(AbstractBaseUser, PermissionsMixin):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.

    Username, password and email are required. Other fields are optional.
    """

    username = models.CharField(_('username'), max_length=100, unique=True,
                                help_text=_('Tələb olunur. 75 simvol və ya az. Hərflər, Rəqəmlər və '
                                            '@/./+/-/_ simvollar.'),
                                # validators=[
                                #     validators.RegexValidator(r'^[\w.@+-]+$', _('Düzgün istifadəçi adı daxil edin.'),
                                #                               'yanlışdır')
                                # ]
                                )
    uuid_id = models.UUIDField(_('uuid'), unique=True, default=uuid.uuid4)
    first_name = models.CharField(_('first name'), max_length=255, blank=True)
    last_name = models.CharField(_('last name'), max_length=255, blank=True)
    email = models.EmailField(_('email address'), max_length=255,unique=True)
    birthdate = models.DateField(verbose_name="ad günü",blank=True,null=True)
    profile_picture = models.ImageField(upload_to=path_and_rename, null=True, blank=True)
    verified = models.BooleanField(default=False, verbose_name=_("Confirming"))
    phone = models.CharField(max_length=100, verbose_name="Telefonu", null=True, blank=True)
    facebook = models.URLField(null=True, blank=True)
    linkedin = models.URLField(null=True, blank=True)
    instagram = models.URLField(null=True, blank=True)
    about = models.TextField(null=True, blank=True)
    slug = models.SlugField(null=True, blank=True)
    sections = models.ManyToManyField('content.Section',blank=True,null=True,)
    profile_photo = models.ForeignKey('content.ProfilePhoto',blank=True,null=True,on_delete=models.CASCADE)


    image = models.ForeignKey(Image,related_name="+",on_delete=models.CASCADE,blank=True,null=True)
    cropping = ImageRatioField('image__image_field', '500x500')


    usertype = models.IntegerField(choices=USERTYPES, verbose_name="User Type", default=2)
    is_staff = models.BooleanField(_('staff status'), default=False,
                                   help_text=_('Designates whether the user can log into this admin '
                                               'site.'))
    is_active = models.BooleanField(_('active'), default=True,
                                    help_text=_('Designates whether this user should be treated as '
                                                'active. Unselect this instead of deleting accounts.'))
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    """
        Important non-field stuff
    """
    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('Users')

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()
    def get_profile_photos(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        return ProfilePhoto.objects.filter(user=self).order_by('-created_date')

    def get_short_name(self):
        "Returns the short name for the user."
        return self.first_name
    def get_user_type(self):
        "Returns the short name for the user."
        return USERTYPESDICT['{}'.format(self.usertype)]

    def save(self, *args, **kwargs):
        super(MyUser, self).save(*args, **kwargs)
        self.slug = slugify(self.first_name+str(timezone.now().timestamp()).replace('.','-'))
        super(MyUser, self).save(*args, **kwargs)
    # def permissions(self):
    #     return_list = []
    #     for item in UserPermission.objects.filter(user=self):
    #         return_list.append(item.permission)
    #     return return_list


class UserConfrimationKeys(models.Model):
    key = models.CharField(max_length=255,null=True, blank=True)
    user = models.ForeignKey('MyUser', null=True,blank=True,on_delete=models.CASCADE)
    expired = models.BooleanField(default=False)
    date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('-date',)
        verbose_name = _("Confirmed user")
        verbose_name_plural = _("Confirmed users")

    def __str__(self):
        return "%s" % self.key


#
# class UserPermission(models.Model):
#     user = models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.CASCADE)
#     permission = models.CharField(choices=rule_models_choices,max_length=255)
#     class Meta:
#         unique_together = ("user", "permission")


