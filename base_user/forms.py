import os
# from nocaptcha_recaptcha.fields import NoReCaptchaField
from django import forms
from django.contrib.auth import get_user_model
from django.forms import ModelForm
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.forms import ReadOnlyPasswordHashField, AuthenticationForm
from django.contrib.auth import authenticate
from nocaptcha_recaptcha import NoReCaptchaField

from base_user.tools.common import FORMUSERTYPES

User = get_user_model()
from PIL import Image

# from home.models import *

GENDER_CHOICE = (
    (1,"Male"),
    (2,"Female"),
    (3,"Other"),
)

class LoginForm(forms.Form):
    lemail = forms.EmailField(required=True,label=_('Email'),widget=forms.EmailInput(attrs={'autocomplete':'off','class': 'form-control','placeholder': '',}))
    lpassword = forms.CharField(max_length=255,label=_('Password'),required=True,widget=forms.PasswordInput(attrs={'autocomplete':'off','class': 'form-control','placeholder': '',}))
    # captcha = NoReCaptchaField()
    remember_me = forms.BooleanField(required=False,widget=forms.CheckboxInput(attrs={'autocomplete':'off','class': '',}))


class MyUserCreationForm(forms.ModelForm):
    """
    A form that creates a user, with no privileges, from the given email and
    password.
    """
    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
    }
    password1 = forms.CharField(label=_("Password"),
                                widget=forms.PasswordInput)
    password2 = forms.CharField(label=_("Password confirmation"),
                                widget=forms.PasswordInput,
                                help_text=_("Enter the same password as above, for verification."))

    class Meta:
        model = User
        fields = ("username", "email", "first_name", "last_name")

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2

    def save(self, commit=True):
        user = super(MyUserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class MyUserChangeForm(forms.ModelForm):
    password = ReadOnlyPasswordHashField(label=_("Password"),
                                         help_text=_(
                                             "Raw şifrələr bazada saxlanmır, onları heç cürə görmək mümkün deyil "
                                             "bu istifadəçinin şifrəsidir, lakin siz onu dəyişə bilərsiziniz "
                                             " <a href=\"../password/\">bu form</a>. vasitəsilə"))

    class Meta:
        model = User
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(MyUserChangeForm, self).__init__(*args, **kwargs)
        f = self.fields.get('user_permissions', None)
        if f is not None:
            f.queryset = f.queryset.select_related('content_type')

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]


"""
    ################################################################################################
    Base System For Client Login -----------------------------------------------------------------------
    ################################################################################################
"""


class BaseAuthenticationForm(AuthenticationForm):
    username = forms.CharField(max_length=254, label="İstifadəçi adı")
    password = forms.CharField(label="Parol", widget=forms.PasswordInput())

    def __init__(self, *args, **kwargs):
        super(BaseAuthenticationForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget = forms.EmailInput(attrs={
            'placeholder': _("E-poçt"), 'class': 'form-control form-project', 'id': 'user_email', 'autofocus': None})
        self.fields['password'].widget = forms.PasswordInput(attrs={
            'placeholder': _("Şifrə"), 'class': 'form-control form-project', 'id': 'user_password'})

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        if not user:
            raise forms.ValidationError("Təəsüflər olsunki email və ya şifrə yanlışdır")
        return user


class UserRegistrationBaseView(forms.Form):
    name = forms.CharField(max_length=254, label="Ad", error_messages={})
    surname = forms.CharField(max_length=254, label="Soy ad", error_messages={})
    email = forms.EmailField(error_messages={})
    password = forms.CharField(label=_("Password"))
    retype_password = forms.CharField(label=_("Password confirm"))

    def __init__(self, *args, **kwargs):
        super(UserRegistrationBaseView, self).__init__(*args, **kwargs)
        self.fields['name'].widget = forms.TextInput(attrs={
            'placeholder': _("Ad")})
        self.fields['surname'].widget = forms.TextInput(attrs={
            'placeholder': _("Soy ad")})
        self.fields['email'].widget = forms.EmailInput(attrs={
            'placeholder': _("Email")})
        self.fields['password'].widget = forms.PasswordInput(attrs={
            'placeholder': _("Şifrə")})
        self.fields['retype_password'].widget = forms.PasswordInput(attrs={
            'placeholder': _("Şifrə təkrarı")})

    def clean(self):
        cleaned_data = super(UserRegistrationBaseView, self).clean()

        password = cleaned_data.get('password')
        password_confirm = cleaned_data.get('retype_password')

        if password and password_confirm:
            if password != password_confirm:
                raise forms.ValidationError(_("Şifrələr eyni deyil"))
        return cleaned_data

    def save(self, commit=True):
        name = self.cleaned_data.get('name')
        surname = self.cleaned_data.get('surname')
        username = self.cleaned_data.get('email')
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')
        if commit:
            user = User.objects.create_user(
                first_name=name,
                last_name=surname,
                email=email,
                username=username,
                password=password,
            )
            user.is_active = False
            user.save()
            return user
        else:
            return forms.ValidationError(_("Password beraber deyil"))

CHOSE = (
    (1, "Kişi"),
    (2, "Qadın")
)



class UserRegistrationForm(forms.Form):
    name = forms.CharField(required=True,max_length=254, label="Ad", error_messages={})
    surname = forms.CharField(required=True,max_length=254, label="Soy ad", error_messages={})
    email = forms.EmailField(required=True,error_messages={})

    phone = forms.CharField(required=True,error_messages={}, widget=forms.TextInput(attrs={'placeholder':'Nömrə'}))
    usertype = forms.ChoiceField(label=_('İstifadəçi tipi'),choices=FORMUSERTYPES,required=True,widget=forms.Select(attrs={
            'class':'form-control'}))
    password = forms.CharField(required=True,label=_("Şifrə"))
    retype_password = forms.CharField(required=True,label=_("Şifrə təkrarı"))
    agree = forms.BooleanField(required=True,label=_("Razısnız"))
    # captcha = NoReCaptchaField()


    def __init__(self, *args, **kwargs):
        super(UserRegistrationForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget = forms.TextInput(attrs={
            'class':'form-control'})
        self.fields['surname'].widget = forms.TextInput(attrs={
            'class':'form-control'})
        self.fields['email'].widget = forms.EmailInput(attrs={
            'class':'form-control'})
        self.fields['phone'].widget = forms.TextInput(attrs={
            'class':'form-control'})
        self.fields['password'].widget = forms.PasswordInput(attrs={
            'class':'form-control'})
        self.fields['retype_password'].widget = forms.PasswordInput(attrs={
            'class':'form-control'})
        self.fields['agree'].widget = forms.CheckboxInput(attrs={
            })

