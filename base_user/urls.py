from django.urls import path


from .views import *

app_name = 'base-user'
urlpatterns = [
	path('login/',  log_in, name='login'),
	path('signup/', signup, name='signup'),
	path('logout/', log_out, name='logout'),
]
